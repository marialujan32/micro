
/**
 * Driver sensor de luz con LDR
 * LDR conectada entre 5V y tierra con una resistencia en serie de 1KOhm. Pin del ADC (IN2) conectado a PA2.
 * Incluye un filtro para evitar el ruido que en este sensor es considerable.
 */
#include "LDR_sensor.h"
#include "stdlib.h"

//Filtro paso banda
const int f_level = 10; //Una diferencia menor que este valor entre el valor medido y el valor anterior se considera ruido.
extern int f_adc_value;// Valor del ADC leído por DMA, canal 2
void filter (int val){
		if(abs(val-f_adc_value)>f_level){ //El valor absoltuto de la diferencia entre la medida actual y anterior es mayor que la franja de ruido
			f_adc_value=val;//Actualizamos el valor
		  }
}

char GetLightLevel(int raw_val){
	char level;
	filter(raw_val); //Filtramos el valor leído desde el ADC
	//Dependiendo de la luminosidad obtenemos uno u otro nivel de luz codificado del 0 al 3.
	int i=f_adc_value;//Valor filtrado del ADC
	if (i<39) //sol
		level = '3';
	else if (i<250) //sombra
		level = '2';
	else if (i<350) //mucha sombra
		level = '1';
	else
		level = '0'; // LDR tapada
	return level;
}

