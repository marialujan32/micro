/**
 * Sensor de humedad capacitivo. Pin de datos conectado al canal IN1 del ADC con el pin PA1
 * Vcc=3-5V
 * Para calibrar el sensor, colocarlo en aire seco y anotar
 * el valor que devuelve esta función. Luego hacer lo mismo
 * sumergiéndolo en agua. Sistituir estos valores en el .c
 * como h_value y l_value tal y como se indica.
 */

#include "SoilMoisture_sensor.h"
#include "stdio.h"
#include "i2c-lcd.h"
//Variables
const int h_value = 974; //Valor obtenido para aire seco
const int l_value = 512; //Valor obtenido para agua

extern ADC_HandleTypeDef hadc1; //Cambiar al canal del ADC empleado

//Ajuste del sensor a porcentaje de humedad en el ambiente
float map_value(int val){

	float m_val;
	m_val = -100*(val-l_value)/(h_value-l_value)+100;; //Ajustamos la escala de medidas
	if (m_val>=100){
		m_val = 100;
	}
	else if (m_val<=0){
		m_val = 0;
	}
	return m_val;//Devolvemos el valor en %
}


float calibrate_Rh(ADC_HandleTypeDef* hadc){
	int32_t adcval;//Valor leído por el ADC
	HAL_ADC_Start(hadc);
	HAL_ADC_PollForConversion(hadc, 100);
	adcval=HAL_ADC_GetValue(hadc);
	HAL_Delay(1000);
	HAL_ADC_Stop(hadc);
	return adcval;
}

float GetRh(ADC_HandleTypeDef* hadc){
	int32_t adcval;//Valor leído por el ADC
	float Humidity = 0;//Humedad en porcentaje
	HAL_ADC_Start(hadc);
		HAL_ADC_PollForConversion(hadc, 100);
		adcval=HAL_ADC_GetValue(hadc);
		HAL_Delay(1000);
	HAL_ADC_Stop(hadc);
	Humidity = map_value(adcval);
	return Humidity;
}

float GetRh_dma(uint32_t i){
	float val;
	val=map_value(i); //Convertimos el valor leído a porcentaje
	return val;
}

//Mostrar en la pantalla LDC la humedad en %
void Display_Rh (float Rh)
{
	char str[20] = {0};
	lcd_put_cur(1, 0); //Segunda línea del LCD

	sprintf (str, "RH:- %.2f ", Rh);
	lcd_send_string(str);
	lcd_send_string("%  ");
}




