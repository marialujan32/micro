/**
 * Driver para el sensor de temperatura.
 * Este sensor emplea el método One-Wire, es decir, un solo pin tanto
 * para leer como para escribir en él. El sensor actúa siempre en modo
 * esclavo, y se debe inicializar cada vez que nos queremos comunicar
 * con él siguiendo lo indicado en su hoja de datos. [ver memoria]
 * PIN DATOS PA3, VCC 3-5.5V
 *
 */
#include "Tp_sensor.h"
#include "stdio.h"
#include "i2c-lcd.h"
//Variables
extern TIM_HandleTypeDef htim10; //[timer usado]
volatile uint8_t Presence =0;

//PIN DE DATOS PA3 [modificar según esté conectado]
#define DS18B20_PORT GPIOA
#define DS18B20_PIN GPIO_PIN_3

void delay (uint16_t time){
	__HAL_TIM_SET_COUNTER(&htim10, 0);
	while((__HAL_TIM_GET_COUNTER(&htim10))<time);
}//Delay en microsegundos


//PIN ENTRADA-SALIDA

void Set_Pin_Output (GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	GPIO_InitStruct.Pin = GPIO_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOx, &GPIO_InitStruct);
}//Configuración del pin de datos como salida

void Set_Pin_Input (GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	GPIO_InitStruct.Pin = GPIO_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(GPIOx, &GPIO_InitStruct);
}//Configuración del pin de datos como entrada



//INICIALIZACIÓN
/**Transmitir un pulso de reset de 480 us desde el dispositivo maestro y leer
 *  un pulso de presencia desde el dispositivo esclavo
 */

uint8_t DS18B20_Start (void)
{
	uint8_t Response = 0;
	Set_Pin_Output(DS18B20_PORT, DS18B20_PIN); //Pin de salida
	HAL_GPIO_WritePin (DS18B20_PORT, DS18B20_PIN, 0);//Pin a 0
	delay (480); //Delay según la hoja de datos

	Set_Pin_Input(DS18B20_PORT, DS18B20_PIN);  //Pin entrada
	delay (80); //Delay según la hoja de datos

	if (!(HAL_GPIO_ReadPin (DS18B20_PORT, DS18B20_PIN))) Response = 1;   //Pin a 0, pulso detectado
	else Response = -1;//Error, pulso de presencia no detectado

	delay (400);

	return Response;
}

//ESCRITURA

void DS18B20_Write (uint8_t data)
{
	Set_Pin_Output(DS18B20_PORT, DS18B20_PIN);  //Pin como salida

	for (int i=0; i<8; i++)
	{

		if ((data & (1<<i))!=0) //Si el bit es 1 (1&1=1, 1&0=0)
		{
			// Escribimos 1: poner a 0 y soltar el bus tras 15 us

			Set_Pin_Output(DS18B20_PORT, DS18B20_PIN); //Pin como salida
			HAL_GPIO_WritePin (DS18B20_PORT, DS18B20_PIN, 0);  //Lo ponemos a 0
			delay (1);  //esperamos 1us

			Set_Pin_Input(DS18B20_PORT, DS18B20_PIN);  // Pin como entrada
			delay (60);  //esperamos 60 us
		}

		else  // Si el bit es 0
		{
			// Escribimos 0: poner a 0 y mantener al menos 60us

			Set_Pin_Output(DS18B20_PORT, DS18B20_PIN);
			HAL_GPIO_WritePin (DS18B20_PORT, DS18B20_PIN, 0);  // Lo ponemos a 0
			delay (60);  // esperamos 60us

			Set_Pin_Input(DS18B20_PORT, DS18B20_PIN);
		}
	}
}

//LECTURA

uint8_t DS18B20_Read (void)
{
	uint8_t value=0;

	Set_Pin_Input(DS18B20_PORT, DS18B20_PIN); //Pin como entrada

	for (int i=0;i<8;i++)
	{
		Set_Pin_Output(DS18B20_PORT, DS18B20_PIN);   // Pin como salida

		HAL_GPIO_WritePin (DS18B20_PORT, DS18B20_PIN, 0);  // Pin de datos a 0
		delay (1);  // esperamos 1us

		Set_Pin_Input(DS18B20_PORT, DS18B20_PIN);  // Pin como entrada
		if (HAL_GPIO_ReadPin (DS18B20_PORT, DS18B20_PIN))  // si el pin es 1
		{
			value |= 1<<i;  // leemos 1 y lo almacenamos en value
		}
		delay (60);  // esperamos 60us
	}
	return value; //devolvemos el valor
}

//LEER TEMPERATURA

float GetTp(void){
	uint16_t  TEMP;
	uint8_t Temp_byte1, Temp_byte2;
	float Temperature =0;

	Presence = DS18B20_Start ();
	HAL_Delay (1);
	DS18B20_Write (0xCC);  // skip ROM: sin selección de dispositivo esclavo
	DS18B20_Write (0x44);  // convert T: inicializa la conversión de tp
	HAL_Delay (800);

	Presence = DS18B20_Start ();
	HAL_Delay(1);
	DS18B20_Write (0xCC);  // skip ROM: sin selección de dispositivo esclavo
	DS18B20_Write (0xBE);  // Read Scratch-pad: Lectura de la memoria del sensor

	//La temperatura se almacena en un registro de dos bytes de la SRAM
	Temp_byte1 = DS18B20_Read();
	Temp_byte2 = DS18B20_Read();
	//Combinación de los dos bytes
	TEMP = (Temp_byte2<<8)|Temp_byte1;
	Temperature = (float)TEMP/16; //Lo convertimos a float
	return Temperature;
}

//SALIDA POR PANTALLA LCD
void Display_Temp (float Temp)
{
	char str[20] = {0};
	lcd_put_cur(0, 0);
	sprintf (str, "TEMP: %.2f ", Temp);
	lcd_send_string(str);
	lcd_send_data('C');

}
