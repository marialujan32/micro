#include "Climas.h"
#include "Actuadores.h"

extern int usermode;
extern unsigned char userParam[4];

param Desierto = {'3','1'};
param Mediterraneo ={'1','2'};
param Tropico = {'3','3'};
param Default={'0','0'};
//extern param clima;
void set_parameters(param* clima){
	switch (clima->tp){
		case '1':{
			clima->tp_min=15;
			clima->tp_max=20;
		}break;
		case '2':{
			clima->tp_min=22;
			clima->tp_max=27;
		}break;
		case '3':{
			clima->tp_min=28;
			clima->tp_max=35;
		}break;

		default:break;
	}

	switch (clima->rh){
		case '1':{
			clima->rh_min=0;
			clima->rh_max=10;
		}break;
		case '2':{
			clima->rh_min=40;
			clima->rh_max=60;
		}break;
		case '3':{
			clima->rh_min=85;
			clima->rh_max=100;
		}break;

		default:break;
	}

}
// float rh, float temp, char light
void Update(unsigned char key, param* clima){
	switch (key){
	case'D':  *clima = Desierto;break;

	case'M':  *clima = Mediterraneo;break;

	case'T':  *clima = Tropico;break;

	case 'P': //User: seleccionado por el usuario
			usermode = 1;break;
	case 'd':{
		*clima = Default;
		Reset();
	}break;
	//Interruptores app
	case 'V': Set_Vent(1); break;
	case 'v': Set_Vent(0); break;
	case 'U': Set_UV_Leds(1); break;
	case 'u': Set_UV_Leds(0); break;
	case 'L': Set_Lamp(1); break;
	case 'l': Set_Lamp(0); break;
	case 'R': Water(3000);break;
	case 'r': Aspersor(2000);break;
	case 'A': clima->Light='1'; break;
	case 'a': {
		clima->Light='0';
		Set_UV_Leds(0);
	}break;

	}//switch
	if (userParam[0]=='P'){
		 clima->tp = userParam[1];
		 clima->rh = userParam[2];
		 userParam[0]='0';
	}
	set_parameters(clima);
}
void Monitor(param* clima, float tp, float rh,char light){
	//Control temperatura
		  if (clima->tp!='0'){
			  if(tp>clima->tp_max) Set_Vent(1);
			  else Set_Vent(0);
			  if (tp<clima->tp_min) Set_Lamp(1);
			  else Set_Lamp(0);
		  }
	//Control humedad ambiente
		  if (clima->rh!='0'){
			  if(rh<clima->rh_min)Set_Aspersor(1);
			  else Set_Aspersor(0);
		  }
	//Control luz
		  if (clima->Light=='1'){
			Auto_UVLeds(1, light);
		  }
}
void Reset(void){
	Set_Lamp(0);
	Set_Aspersor(0);
	Set_Vent(0);
}

