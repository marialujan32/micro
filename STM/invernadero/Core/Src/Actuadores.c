/**
 * Driver de todos los actuadores del invernadero. Función Toggle y on-off on durante un intervalo de tiempo
 * a elegir en milisegundos.
 * Las salidas cierran los mosfets conectados al circuito de potencia externo que alimenta los actuadores.
 *
 * LEDs UV -> PD12
 * Lámpara de calor -> PD13
 * Bomba de agua -> PD14
 * Ventilador -> PD15
 *
 */
#include "Actuadores.h"
extern TIM_HandleTypeDef htim11; //temporizador usado
extern unsigned char sistemParam; //Variable desde el main.c

void delay_act (uint16_t time){
	__HAL_TIM_SET_COUNTER(&htim11, 0);
	while((__HAL_TIM_GET_COUNTER(&htim11))<time*10); //((96Mhz/9600)^-1)*10= t en ms
}//Delay en milisegundos

//TIRA DE LEDS UV

void Toggle_UVLeds(){
	HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_12);
}

void Set_UV_Leds(int key){
	if (key ==1)
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12,GPIO_PIN_SET);
	else if (key ==0)
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12,GPIO_PIN_RESET);
}
//Funcionamiento automático en función del nivel de luz
void Auto_UVLeds(int key,char luz){
	if(key==1){
		if((luz =='2')||(luz=='1')){ //Encendido para sombra, apagado para sol y noche
			Set_UV_Leds(1);
		}
		else Set_UV_Leds(0);
	}
}

//LÁMPARA DE CALOR

void Set_Lamp(int key){
	if (key ==1)
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13,GPIO_PIN_SET);
	else if (key ==0)
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13,GPIO_PIN_RESET);
}

void Toggle_Lamp(){
	HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_13);
}

//BOMBA DE AGUA

void Set_Aspersor (int key){
	if (key == 1)
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14,GPIO_PIN_SET);
	else if (key == 0)
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14,GPIO_PIN_RESET);
}
//Intervalo de riego configurable
void Aspersor(int time){
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14,GPIO_PIN_SET);
	delay_act(time); //Esperamos el tiempo configurado
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14,GPIO_PIN_RESET);
	sistemParam='0';

}
//Mismo actuador pero se podría añadir una segunda bomba de agua. Funcionalidad extra
void Water(int time){
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14,GPIO_PIN_SET);
	delay_act(time); //Esperamos el tiempo configurado
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14,GPIO_PIN_RESET);
	sistemParam='0';
}

//VENTILADOR

void Set_Vent (int key){
	if (key == 1)
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15,GPIO_PIN_SET);
	else if (key == 0)
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15,GPIO_PIN_RESET);
}

void Toggle_Vent(){
	HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_15);
}
//Intervalo de ventilación configurable
void Ventilation(int time){
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15,GPIO_PIN_SET);
	delay_act(time); //Esperamos el tiempo configurado
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15,GPIO_PIN_RESET);
	sistemParam='0';
}
