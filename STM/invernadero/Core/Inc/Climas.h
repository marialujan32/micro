// Estructura de clima
typedef struct{
	char tp;
	char rh;
	int tp_min;
	int tp_max;
	int rh_min;
	int rh_max;
	char Light;
}param;

void set_parameters(param* clima); //Se actualizan los parámetros según el clima seleccionado
void Update(unsigned char key, param* clima); //Se actuaizan los valores dependiendo del comando recibido
void Monitor(param* clima, float tp, float rh, char light); //Se monitorizan las condiciones ambientales
void Reset(void);
