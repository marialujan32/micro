/**
 * Sensor de humedad capacitivo. Pin de datos conectado al canal IN1 del ADC con el pin PA1
 * Vcc=3-5V
 * Para calibrar el sensor, colocarlo en aire seco y anotar
 * el valor que devuelve esta función. Luego hacer lo mismo
 * sumergiéndolo en agua. Sistituir estos valores en el .c
 * como h_value y l_value tal y como se indica.
 */

#include "stm32f4xx_hal.h"

float map_value(int val); //Ajuste del sensor a porcentaje de humedad en el ambiente

float calibrate_Rh(ADC_HandleTypeDef* hadc); //Calibración del sensor

float GetRh(ADC_HandleTypeDef* hadc);//Lectura del valor de humedad en %
float GetRh_dma(uint32_t i);//Lectura del valor de humedad en % por DMA

void Display_Rh (float Rh); //Mostrar por pantalla LCD
