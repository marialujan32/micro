/**
 * Driver para el sensor de temperatura.
 * Este sensor emplea el método One-Wire, es decir, un solo pin tanto
 * para leer como para escribir en él. El sensor actúa siempre en modo
 * esclavo, y se debe inicializar cada vez que nos queremos comunicar
 * con él siguiendo lo indicado en su hoja de datos. [ver memoria]
 * PIN DATOS PA3, VCC 3-5.5V
 *
 */
#include "stm32f4xx_hal.h"

void delay (uint16_t time); //Crea un delay de microsegundos
//Configuración del pin como entrada/salida
void Set_Pin_Output (GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin);
void Set_Pin_Input (GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin);
//Leer/escribir datos en la SRAM
uint8_t DS18B20_Read (void);
void DS18B20_Write (uint8_t data);

//Medir temperatura (ºC)
float GetTp(void);

//Mostrar en la pantalla LCD la temperatura
void Display_Temp (float Temp);


