/**
 * Driver de todos los actuadores del invernadero. Función Toggle y on-off on durante un intervalo de tiempo
 * a elegir en milisegundos.
 * Las salidas cierran los mosfets conectados al circuito de potencia externo que alimenta los actuadores.
 *
 * LEDs UV -> PD12
 * Lámpara de calor -> PD13
 * Bomba de agua -> PD14
 * Ventilador -> PD15
 *
 */
#include "stm32f4xx_hal.h"

void delay_act (uint16_t time);

void Toggle_UVLeds(void);
void Set_UV_Leds(int key);
void Auto_UVLeds(int key,char luz);

void Set_Lamp(int key);
void Toggle_lamp(void);

void Set_Aspersor (int key);
void Aspersor(int time);
void Water(int time);

void Set_Vent (int key);
void Toggle_Vent(void);
void Ventilation(int time);
