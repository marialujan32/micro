/**
 * Driver sensor de luz con LDR
 * LDR conectada entre 5V y tierra con una resistencia en serie de 1KOhm. Pin del ADC (IN2) conectado a PA2.
 * Incluye un filtro para evitar el ruido que en este sensor es considerable.
 */
void filter (int val); //Filtro anti-ruido
char GetLightLevel(int raw_val); //Nivel de luz

