#https://likegeeks.com/es/ejemplos-de-la-gui-de-python/#Crear_tu_primera_aplicacion_GUI
#TODO:LEER INFOR DESDE EL MICRO
from os import stat_result
import tkinter as tk
from tkinter import ttk
from tkinter import *
from tkinter import messagebox

class Switch (tk.Button):
    state = False
    def __init__(self, parent, column, row, id, **Kwargs):
        super().__init__(parent, **Kwargs, command = self.toggle)
        self.grid(column = column, row = row)
        self.id = id
    def toggle(self):
        if self.state:
            self.state=False
            self.config(relief="raised")
        else:
            self.state = True
            self.config(relief="sunken")
        print (self.state)
    def SetSerialPort(self, x):
        self.serialport = x
    #Comprueba el estado y lo manda al micro
    def SendData(self):  
        if self.state:
            state = 'H'
        else:
            state = 'L'
       #Manda el id del botón y su estado
        self.serialport.write(bytes(self.id, 'UTF-8'))
        self.serialport.write(bytes(state, 'UTF-8'))
    


class Desplegable(ttk.Combobox):
    
    def __init__(self, parent, column, row, callback, **Kwargs):
        super().__init__(parent, **Kwargs, state = "disabled")#Estado disabled por defecto
        self.grid(column=column, row=row)
        self.callback = callback
        self.bind("<<ComboboxSelected>>", self.callback) 


    def GetSelect(self):
        selec = self.current()
        return selec #Devuelve el número de opción elegida
    def GetVal(self):
        val = self.get()
        return val #Devuelve la opción elegida
       



class Rboton(tk.Radiobutton):    
    selected = False  
    def __init__(self, parent, column, row, **Kwargs):
        super().__init__(parent, **Kwargs)
        self.grid(column=column, row=row)
    def clicked (self): #Cambia el estado del botón
        if self.selected:
                self.selected = False
                print (self.selected)
        else:
                self.selected = True
                print (self.selected)
    



class APK (tk.Tk):
   
    def __init__(self,*args, **Kwargs):
        super().__init__(*args, **Kwargs)
        self.geometry("892x555")
        self.option = IntVar() #Botón de modo seleccionado

        self.create_widgets()
        
       
        
    
    def create_widgets(self):
        #Desplegables con los parámetros del invernadero
        self.ClimaOp = Desplegable(self, 1, 0, self.callbackClima, values=["", "Tropical", "Desértico", "Mediterráneo"]) #Tipo de clima
        self.TpOp = Desplegable(self, 1, 1, self.CallbackTpOp, values =["","Cálido", "Medio", "Frío"]) #Temperatura
        self.HOp = Desplegable(self, 2, 1, self.CallbackHOp, values=["", "Húmedo", "Medio", "Seco"]) #Humedad

        #Botones de selcción de modo
        self.Opcion = Rboton(self, 0, 0, text = "Clima", value = 1, variable = self.option, command = self.interaccion)
        self.Opcion = Rboton(self, 0, 1, text = "Tp y H", value = 2, variable = self.option, command = self.interaccion)

        #Botón de envío de datos
        self.sendb = Button(self, text = "APPLY CHANGES", command =self.clicked)
        self.sendb.grid(column=2, row=2)

        #Botones
        self.UVlight = Switch(self, 1, 3, 'U', text = "UV")
        self.Alight = Switch(self, 2, 3, 'A', text = "AMBIENTE")
        self.Hlight = Switch(self, 3, 3, 'H',text = "CALOR")

        

#CALLBACKS de los desplegables: set de la temperatura y humedad
    def callbackClima(self, event):
        x = self.ClimaOp.current()
        if x == 1:
            self.tp=1
            self.h=1
        if x == 2:
            self.tp=2
            self.h=2
        if x==3:
            self.tp=3
            self.h=3
        print(self.tp, self.h)
    def CallbackTpOp(self, event):
        print("tp")
        #DATOS TP
    def CallbackHOp(self, event):
        #DATOS HUMEDAD
        print("h")


    #Acción del botón de selección de modo: impide seleccionar los desplegables de la casilla no marcada
    def interaccion(self):
        x = self.option.get()
        if x==1:
            self.ClimaOp.config(state="readonly")
        else:
            self.ClimaOp.config(state="disable")
            self.ClimaOp.current(0)
 
        if x==2:
            self.TpOp.config(state="readonly")
            self.HOp.config(state="readonly")
        else:
            self.TpOp.config(state="disable")
            self.HOp.config(state="disable")
            self.TpOp.current(0)
            self.HOp.current(0)

    #Callback del botón de enviar cambios
    def clicked(self):
        x= messagebox.askokcancel(message = "¿Cambiar los parámetros?")
        if x==True:
            print ("DATA SENT")
            self.SendData()


    #COMUNICACION SERIE
    #Permite elegir el puerto serie
    def SetSerialPort(self, x):
        self.serialport = x
    #Envío de información en forma de char
    def SendData(self):  
       #Manda 2 bytes, el grado de humedad y temperatura
        self.serialport.write(bytes(self.tp, 'UTF-8'))
        self.serialport.write(bytes(self.h, 'UTF-8'))

      



 
#App SET
app = APK()


app.mainloop()